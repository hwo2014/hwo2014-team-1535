import json
import socket
import logging
from datetime import datetime
from logger import log

class LogFilter(object):
    def __init__(self, levels):
        self.__levels = levels

    def filter(self, logRecord):
        return logRecord.levelno in self.__levels  # only log if loglevel is in list of allowed levels

# prepare logger
log = logging.getLogger()
log.setLevel(logging.NOTSET)
file_handler = logging.FileHandler('../logs/race_%s.log' % datetime.now().strftime('%Y%m%d_%H-%M-%S'))
file_handler.addFilter(LogFilter([logging.INFO]))
stream_handler = logging.StreamHandler()  # logs to sys.stderr
stream_handler.addFilter(LogFilter([logging.WARNING, logging.DEBUG]))
log.addHandler(file_handler)
log.addHandler(stream_handler)


class NoobBot(object):
    car_color = ""

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_your_car(self, data):
        print("we have a %s car... Yay!!" % data["color"])
        self.car_color = data["color"]

    def on_car_positions(self, data):
        ## lets gather some data:
        log.info(data[0])

        self.throttle(0.5)

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'yourCar': self.on_your_car,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
