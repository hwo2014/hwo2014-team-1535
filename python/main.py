import json
import socket
import sys

from noobbot import NoobBot
from averagebot import AverageBot

Bot = AverageBot
#Bot = NoobBot

if __name__ == "__main__":
    if sys.argv[1] == "test":
        import tests
        tests = {
            "max_speed": tests.max_speed
        }
        tests[sys.argv[2]](sys.argv[3:])
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = Bot(s, name, key)
        bot.run()
