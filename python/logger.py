import logging
from datetime import datetime

class LogFilter(object):
    def __init__(self, levels):
        self.__levels = levels

    def filter(self, logRecord):
        return logRecord.levelno in self.__levels  # only log if loglevel is in list of allowed levels


# prepare logger
log = logging.getLogger()
log.setLevel(logging.NOTSET)
file_name = '../logs/race_%s.log' % datetime.now().strftime('%Y%m%d_%H-%M-%S')
file_handler = logging.FileHandler(file_name)
file_handler.addFilter(LogFilter([logging.INFO]))
stream_handler = logging.StreamHandler()  # logs to sys.stderr
stream_handler.addFilter(LogFilter([logging.WARNING, logging.DEBUG]))
log.addHandler(file_handler)
log.addHandler(stream_handler)
