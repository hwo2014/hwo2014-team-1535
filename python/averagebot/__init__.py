import json
import socket
import sys
import pickle
from datetime import datetime
from logger import log, file_name as log_file_name


class AverageBot(object):
    """
        AverageBot simply increases throttle until it falls off at some piece. Then it documents the speed it fell off
            can serve for learning purposes (i.e. max speed per piece / position of flag / lane (TODO: maybe inner or outer lanes behave differently?!)
    """
    # the following is not thread-safe but we dont care
    car_color = ""
    dimensions = {  # dimensions of the car, filled on_game_init
        "length": 0,
        "width": 0,
        "guide_flag_position": 0
    }
    track = {  # we fill this with info on_game_init
        "id": "",  # id of track
        "name": "",  # name of track
        "pieces": [],  # different pieces in the track
        "lanes": [],  # where the pieces are
        "max_speeds": []  # a list with maximum throttle that we have learned we can have by piece
    }

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.current = {  # save positions and such info for both cars  TODO: add other players info
            self.name: {
                "throttle": 0.5,  # current throttle, lets start with 0.5
                "angle": 0,  # how much is the car drifting?
                "piece_index": 0,  # which piece are we at?
                "in_piece_distance": 0,  # how fare along the piece are we?
                "start_lane_index": 0,  # current lane index
                "end_lane_index": 0,  # next lane index in case we are before a switch
                "lap": 0,
                "crashed": False
            }
        }

    def get_max_speeds(self):
        with open("max_speeds", "r") as f:
            speeds = pickle.load(f)
        self.track["max_speeds"] = speeds[self.track["id"]] if self.track["id"] in speeds else []
 
    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})
    def throttle(self, throttle):
        self.current[self.name]["throttle"] = throttle
        self.msg("throttle", throttle)

    def adjust_speed(self):
        # set to maximum allowed speed for this piece or 1 if we have not learned max speed yet
        if len(self.track["max_speeds"]) > self.current[self.name]["piece_index"]:  # we have the map saved
            max_speed = self.track["max_speeds"][self.current[self.name]["piece_index"]] if self.track["max_speeds"][self.current[self.name]["piece_index"]] is not None else 1
        else:
            max_speed = 1
        # TODO: slow down early enough before next piece if we have to!
        # TODO: adjust threshold to stay under
        self.throttle(max_speed - 0.01)  # this way the learning rate is 0.01 which may be to slow or to fast
        log.debug("current throttle is: %s\tmax throttle is: %s\tpiece index: %s" % (self.current[self.name]["throttle"], max_speed, self.current[self.name]["piece_index"]))
        #log.debug("overall: index: %s of %s" % (self.current[self.name]["piece_index"], self.track["max_speeds"]))
        self.ping()

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_init(self, data):
        self.read_track(data)  # read the track. Once we know which track we are on we read our track speed
        self.get_max_speeds()

    def read_track(self, data):
        self.track = {
            "id": data["race"]["track"]["id"],  # id of track
            "name": data["race"]["track"]["name"],  # name of track
            "pieces": data["race"]["track"]["pieces"],  # different pieces in the track
            "lanes": data["race"]["track"]["lanes"],  # where the pieces are
            "max_speed": []  # a list with maximum throttle that we have learned we can have by piece
        }
        log.debug("read track: %s" % self.track)
        self.dimensions = {  # TODO: read out the dimensions and flag position of our car!
            "length": 0,
            "width": 0,
            "guide_flag_position": 0
        }

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_your_car(self, data):
        print("we have a %s car... Yay!!" % data["color"])
        self.car_color = data["color"]

    def on_car_positions(self, data):
        #log.debug("got me some data: %s" % data)
        #log.debug("updating self.current: %s " % self.current)
        self.current[self.name].update({
            "angle": data[0]["angle"],  # how much is the car drifting?
            "piece_index": data[0]["piecePosition"]["pieceIndex"],  # which piece are we at?
            "in_piece_distance": data[0]["piecePosition"]["inPieceDistance"],  # how fare along the piece are we?
            "start_lane_index": data[0]["piecePosition"]["lane"]["startLaneIndex"],  # current lane index
            "end_lane_index": data[0]["piecePosition"]["lane"]["endLaneIndex"],  # next lane index in case we are before a switch
            "lap": data[0]["piecePosition"]["lap"]
        })
        #log.debug("now it is: %s " % self.current)
        if not self.current[self.name]["crashed"]:
            self.adjust_speed()
        else:
            self.ping()

    def on_crash(self, data):
        if data["name"] == self.name:
            self.current[self.name]["crashed"] = True
            log.debug("we crashed!! %s" % data)
            log.info(json.dumps(
                {"crash": 
                    {    
                        "throttle": self.current[self.name]["throttle"],
                        "piece_index": self.current[self.name]["piece_index"]
                    }
                }))
            self.current[self.name]["throttle"] = 0
        self.ping()

    def on_spawn(self, data):
        if data["name"] == self.name:
            log.debug("we are back on track")
            self.current[self.name]["crashed"] = False  # TODO: add other bot?

    def on_turbo_available(self, data):
        pass

    def on_game_end(self, data):
        print("Race ended")
        # write max_speed file:
        with open("max_speeds", "r+") as speed_file, open(log_file_name, "r") as log_file:
            self.write_speed_file(speed_file, log_file)
        self.ping()

    def write_speed_file(self, speed_file, log_file):
        speeds = pickle.load(speed_file)
        speed_file.seek(0)  # reset so we can write later
        lines = [json.loads(line) for line in log_file.readlines()]
        crash_lines = filter(lambda line: "crash" in line.keys(), lines)
        speeds[self.track["id"]] = self.update_speeds(crash_lines, speeds[self.track["id"]] if self.track["id"] in speeds else {})
        pickle.dump(speeds, speed_file)

    def update_speeds(self, crash_lines, default):  # returns new maximum speed if we learned that we have to be slower for a given piece
        piece_ids = set([line["crash"]["piece_index"] for line in crash_lines])
        def get_min_throttle(piece_id):
            throttles = [line["crash"]["throttle"] for line in crash_lines if line["crash"]["piece_index"] == piece_id]
            return(min(throttles))

        min_throttle_by_piece = {piece_id: get_min_throttle(piece_id) for piece_id in piece_ids}  # min in the sense that its the smallest value where we crashed
        speeds = [min_throttle_by_piece.get(piece_id, None) for piece_id in range(0, len(self.track["pieces"]))]
        return(speeds)

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'yourCar': self.on_your_car,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'turboAvailable': self.on_turbo_available,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
