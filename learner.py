#!/usr/bin/env python2
"""
    runns a bot continuously. Bots have to take care of logging/data collection
        if <number of repetitions> is not given bots will be spawned indefinitely
"""

import sys
import subprocess
from time import sleep

if len(sys.argv) < 5:
    print("./learner.py <host> <port> <botname> <botkey> [<number of processes>] [<number of repetitions>]")
    sys.exit(1)

num_processes = 1 if len(sys.argv) < 6 else int(sys.argv[5])  # number of running bots, bots run in parallel
reps_left = None if len(sys.argv) < 7 else int(sys.argv[6])  # number of repetitions, infinity if not given

print("starting learner with %d parallel processes, and %d number of repetitions" % (num_processes, float("inf") if reps_left is None else reps_left))

if __name__ == '__main__':
    try:
        processes = [subprocess.Popen(['./run', sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4]]) for i in range(0, num_processes)]
        while reps_left is None or reps_left > 0:  # simple event loop
            statuses = [proc.poll() for proc in processes]
            for i in range(0, num_processes):
                if statuses[i] is not None and reps_left is None or reps_left > 0:
                    if reps_left is not None:
                        reps_left = reps_left - 1
                    processes[i] = subprocess.Popen(['./run', sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4]])
            print("%s repetitions left: %d" % (["Process %d is running" % i if statuses[i] is None else "done" for i in range(0, num_processes)], "infinity" if reps_left is None else reps_left))
    except KeyboardInterrupt:
        print("exiting! cleaning up child processes")
        subprocess.call(["killall", "run"])  # this is very dirty but the clean versions dont seem to work...
